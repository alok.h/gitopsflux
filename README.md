# Flux CD #


### What is this repository for? ###
Flux CD - Bootstrap Flux and GitOps a app Git or app kustomize-helm

### How do I get flux set up? ###
Flux set up. 
* Download flux binary from flux [link](https://fluxcd.io/docs/installation/#install-the-flux-cli)
* check k8s with 'flux check --pre', If ok, please proceed to bootstrap [link](https://fluxcd.io/docs/installation/#generic-git-server)
* to run flux, the pre-req is that the ssh-agent must be working either windows or linux  
* Optionally make a ssh-private-key [link](https://github.com/fluxcd/flux/blob/master/chart/flux/README.md#install-the-chart-with-the-release-name-flux) or bootstrap will prompt for one

```
linux:
------
$ssh-keygen -q -N "" -f ./identity
$ssh-keyscan bitbucket.org > ./known_hosts
$eval `ssh-agent`
$ssh-add ./identity

Windows with powershell in admin mode. pre-req is google-sdk installed, cluster creds downloaded and kubectl able to list pods on the cluster and flux cli installed:
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
set-service ssh-agent StartupType 'Automatic'
Get-Service ssh-agent
ssh-keygen
ssh-add  C:\Users\AlokHom/.ssh/id_rsa

Bootstrap
----------
$flux bootstrap git \
  --url=ssh://git@<host>/<org>/<repository> \
  --commit-message-appendix "[clickup-id] flux" \
  --branch=<main or feature/branchname> \
  --path=clusters/<path-to-app-helm-release-and-repository-files-and-optional-k8s-manifests>
  --private-key-file=<path/to/private.key>
#  --password=<key-passphrase-if-any>
```

How to connect an App Git / Helm repositories to flux ?

App GitOps with flux:
--------------------

Create an App GitRepository. If you want to securely connect the git to the flux, then we need to provide the ssh-credentials 

```
flux create source git calculation-framework \
  --url=ssh://git@bitbucket.org:teampiscadacloud/calculation-framework \
  --branch=main \
  --interval=30s \
  --export > ./clusters/my-cluster/calculation-framework-source.yaml

Edit the ./clusters/my-cluster/calculation-framework-source.yaml file and add the ssh credentials secretRef and apply the file. 
 
  secretRef:
    name: ssh-credentials

pre-req:
-------
kubectl create secret generic ssh-credentials \
    --from-file=./identity \
    --from-file=./identity.pub \
    --from-file=./known_hosts

$kubectl apply -f ./clusters/my-cluster/calculation-framework-source.yaml
```

Next, GitOps the app GitRepository source with a flux kustomize.  
* We need to connect to the ./kustomize path to the directory containing a kustomization.yaml file (default ./) 
  and put in the kustomisation files for the app to periodically reconcile
  e.g. in this repo --path is ./clusters/tools where Nexus app kustomization files are mentioned.  
* One can add new apps by having a folder for app and keep all its kubernetes manifests in it and relevant kustomization file for it. 

```
flux create kustomization calculation-framework-kust
  --target-namespace=default \
  --source=calculation-framework \
  --path="./kustomize" \
  --prune=true \
  --interval=5m \
  --export > ./clusters/my-cluster/calculation-framework-kustomization.yaml

$kubectl apply -f ./clusters/my-cluster/calculation-framework-kustomization.yaml 
```

Helm GitOps with Flux:
---------------------
* Flux Helm repo should have a Clusters/ folder for flux to provide the location of the HelmRelease and HelmRepository manifests to handle helm reconciliations. 
* Ref:[link](https://fluxcd.io/docs/guides/helmreleases/)

```
 | - clusters
     |-<cluster-name>
            |---<app-helm-manifests>
```
You can also manually add a new app Helm chart in your Helm repo and add a HelmRelease 
* Add a chart and make a HelmRepository
* Add a HelmRelease of the app from the chart.
* Add any more relevant k8s manifests (if needed). 
* Ensure you have a namespace either that the chart supplies or explicitly ( e.g.nginx or nexus)


```
flux create source helm bitnami \
  --interval=1h \
  --url=https://charts.bitnami.com/bitnami

flux create helmrelease nginx \
  --interval=1h \
  --release-name=nginx-ingress-controller \
  --target-namespace=nginx \
  --source=HelmRepository/bitnami \
  --chart=nginx-ingress-controller \
  --chart-version="5.x.x"
```

